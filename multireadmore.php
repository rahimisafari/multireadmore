<?php
/**
 * @package multiReadmore
 * @author hossein rahimi safari http://achag.ir
 * @copyright (C) 2019 - 2019 - hosein rahimi safari - http://achag.ir
 * @license GNU/GPLv3 http://www.gnu.org/licenses/gpl-3.0.html
**/

defined( '_JEXEC' ) or die( 'Restricted access' );

jimport( 'joomla.plugin.plugin' );
jimport('joomla.language.helper');

class plgContentMultireadmore extends JPlugin {

	function __construct(&$subject, $config)
	{
		parent::__construct($subject, $config);
	}

	public function onAfterDispatch()
	{		
      
    $app = JFactory::getApplication();
    if ($app->isSite()) {
      $doc = JFactory::getDocument();
      JHtml::_('stylesheet', JUri::root() . 'media/plg_multireadmore/css/multireadmore.css');  // For CSS files
      JHtml::_('script', JUri::root() . 'media/plg_multireadmore/js/multireadmore.js');
      $this->loadLanguage('', JPATH_ADMINISTRATOR);
    }
	}
}
