jQuery(document).ready(function(){
    jQuery('.accordcontent').each(function (index, element) {
        
        jQuery('<div class="acc_container" id="acc_container' + index + '"></div>').insertAfter(this);
        jQuery('#acc_container' + index).append(this);

        jQuery('<button id="acc' + index +
                '" type="button" class="acctheme4 accreadmorebtn">ادامه مطلب</button>')
            .insertAfter(this);
        jQuery(this).attr("id", "accordcontent" + index);

        var first = jQuery(this).offset().top;
        var sec = jQuery(this).find('.accordreadmore').offset().top;
        var distance = parseInt(sec) - parseInt(first);
        var orginalHeight = jQuery(this).height();

        jQuery(this).attr("orginalheight", orginalHeight);
        jQuery(this).css('height', distance);
        jQuery(this).attr("accheight", distance);

        


        //on read more button click---------------------------------
        jQuery('button#acc' + index).click(function () {
            var orginalHeight = jQuery('#accordcontent' + index).attr("orginalheight");
            var newHeight = 0;
            //var buttonText = 'خلاصه مطلب';
            var isCollapsed = false;
            if (jQuery('#accordcontent' + index).height() != orginalHeight) {
                newHeight = orginalHeight;
                isCollapsed = true;
            } else {
                //buttonText = 'ادامه مطلب';
                newHeight = jQuery('#accordcontent' + index).attr("accheight");
            }

            if (isCollapsed) {
                //scroll to element------------------------------
                jQuery('html, body').animate({
                    scrollTop: jQuery('button#acc' + index).offset().top-50
                }, function () {
                    //slide toggle-----------------------------------
                    jQuery('#accordcontent' + index).animate({
                        height: newHeight + "px"
                    }, function () {
                        //jQuery('button#acc' + index).text(buttonText);
                        jQuery('button#acc' + index).animate({
                            opacity: 0
                        }); // css('display','none');
                    });
                });
            } else {

                //for collapse expanded accardeon.disabled temporary.
                /*jQuery('#accordcontent' + index).animate({
                    height: newHeight + "px"
                }, function () {
                    jQuery('html, body').animate({
                        scrollTop: jQuery('#accordcontent' + index).offset().top
                    }, function () {
                        jQuery('button#acc' + index).text(buttonText);
                    });
                });*/
            }
        });
    });
});